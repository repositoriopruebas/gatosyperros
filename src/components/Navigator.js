import { queryAllByAltText } from '@testing-library/react';
import React,{useState,useEffect} from 'react';

const Navigator = (props) =>
{
    const  [selector,setSelector] = useState(1);   
    useEffect(()=>{
           if(selector == 1)
           {
                document.getElementById('1').style.color = 'red';
                document.getElementById('2').style.color = 'greenyellow'; 
           }else
           {
                document.getElementById('1').style.color = 'greenyellow';  
                document.getElementById('2').style.color = 'red';  
           }            
           props.puppy(selector)      
    },[selector])
    return <div>
                <span>
                        <a href="#" className="btn btn-dark" id="1" onClick= {() => setSelector(1)}>Ver Fotos de Gatos </a>
                </span> 
                <span>
                        <a href="#"  className="btn btn-dark" id="2" onClick= {() => setSelector(2)} >Ver Fotos de Perros</a>
                </span>
               
            </div>
    
}
export default Navigator;