import React,  { useState, useEffect,Fragment } from 'react';
import Navigator from './Navigator'
import { getKitties, getDogs } from '../services/connectToApi';

const Content = () =>
{
    const [url,setUrl] = useState('');
    const [kitty,setKitty] = useState({
        name:'',
        origin:'',
        image:''
    });
    const [dog,setDog] = useState("")
    const [time,setTime] = useState(true);
    const [loading,setLoading] = useState(false);
    const  [selector,setSelector] = useState(1);  
    useEffect(() => { 
        if(time == true)
        {
            setLoading(true);              
        }    
        if(selector == 1)
        {
            Kitties();        
        }else
        {
            Dogs();      
        }                      
        return setTime(false)         
    },[selector,time]);
    const SelectionNav = (puppyType) =>
    {
        setSelector(puppyType);
    }
    const Dogs = () =>
    {
       getDogs().then(data =>{
            console.log(data);
            setDog(data.message);
       }); 
    }
    const Kitties = () =>
    {
        getKitties().then(data =>{
            setUrl(data[0].url);
            setLoading(false) 
            if(data[0].breeds.length > 0)
            {
               setKitty({
                   name:data[0].breeds[0].name,
                   origin:data[0].breeds[0].origin,
                   temperament:data[0].breeds[0].temperament
               }) 
               console.log(kitty.image)    
            }else
            {
                setKitty({name:'',origin:'',temperament:''})                        
            }
        }); 
    }
    return (
        <Fragment>
            <h2>{(selector === 1)? "Fotos de Gatos" : "Fotos de Perros"}</h2>
            <Navigator puppy={SelectionNav}/>  
                { (selector ===1)? 
                <div>
                     
                     <button type="button" className="btn btn-success" onClick={ () => setTime(true)}>Cambiar de imagen </button>
                     {(kitty.name) ? <h2>Name: {kitty.name} <span>Origin: {kitty.origin}</span> </h2>: <h2>Sin datitos <span>No sabemos </span></h2>}              
                     {(loading == true)? <img className="img-responsive img-fluid" src="https://www.hebergementwebs.com/image/74/746be6ba853c6c25d245f0ee24dc2f9c.gif/animations-de-loading-css-how-to-the-examples-4.gif"/> : <img  className="img-responsive img-fluid" src={url}/> }
                </div>                    
                : <div>
                    
                    <button className="btn btn-success" type="button" onClick={ () => setTime(true)}>Cambiar de imagen </button>
                    <h2>Sin datitos <span>No sabemos </span>  </h2>      
                    <img  className="img-responsive img-fluid" src={dog}/> 
                 </div>
           }
                    
                
                    
              
             
        </Fragment>
      
    )
}
export default Content;