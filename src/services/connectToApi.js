export async function getKitties()
{
    const res = await fetch('https://api.thecatapi.com/v1/images/search');    
    return await res.json();
}
export async function getDogs()
{
    const res = await fetch('https://dog.ceo/api/breeds/image/random');    
    return await res.json();
}